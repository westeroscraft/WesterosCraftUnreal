// Copyright 2015, WesterosCraft <http://www.westeroscraft.com>. All Rights Reserved. 

using UnrealBuildTool;
using System.Collections.Generic;

public class WesterosCraftTarget : TargetRules
{
	public WesterosCraftTarget(TargetInfo Target) : base (Target)
	{
        Type = TargetType.Game;

        ExtraModuleNames.Add("WesterosCraft");
	}
}
