// Copyright 2015, WesterosCraft <http://www.westeroscraft.com>. All Rights Reserved.

#pragma once

#include "ModuleManager.h"

class IAnvilGenPlugin : public IModuleInterface
{
public:

	static inline IAnvilGenPlugin& Get()
	{
		return FModuleManager::LoadModuleChecked< IAnvilGenPlugin >("AnvilGen");
	}

	static inline bool isAvailable()
	{
		return FModuleManager::Get().IsModuleLoaded("AnvilGen");
	}
};