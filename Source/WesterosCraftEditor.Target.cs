// Copyright 2015, WesterosCraft <http://www.westeroscraft.com>. All Rights Reserved. 

using UnrealBuildTool;
using System.Collections.Generic;

public class WesterosCraftEditorTarget : TargetRules
{
	public WesterosCraftEditorTarget(TargetInfo Target) : base (Target)
	{
        Type = TargetType.Editor;

        ExtraModuleNames.Add("WesterosCraft");
	}
}
